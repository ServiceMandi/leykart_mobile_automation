package com.leykart.utils;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class ExcelInputData {

	
	//LeyKart Variable
	public String MobileNumber;
	public String LoginTypeName;
	public String Name;
	public String RgistrationNumber;
	public String ChassisNumber;
	public String Aggregates;
	public String ModelLookup;
	public String PartNumber;
	
	
    //LeyKart Method
	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String MobileNumber1) {
		this.MobileNumber = MobileNumber1;
	}

	public String getLoginTypeName() {
		return LoginTypeName;
	}

	public void setLoginTypeName(String LoginTypeName) {
		this.LoginTypeName = LoginTypeName;
	}
	
	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String getRgistrationNumber() {
		return RgistrationNumber;
	}

	public void setRgistrationNumber(String RgistrationNumber) {
		this.RgistrationNumber = RgistrationNumber;
	}

	public String getChassisNumber() {
		return ChassisNumber;
	}

	public void setChassisNumber(String ChassisNumber) {
		this.ChassisNumber = ChassisNumber;
	}

	public String getAggregates() {
		return Aggregates;
	}

	public void setAggregates(String Aggregates) {
		this.Aggregates = Aggregates;
	}

	public String getModelLookup() {
		return ModelLookup;
	}

	public void setModelLookup(String ModelLookup) {
		this.ModelLookup = ModelLookup;
	}

	public String getPartNumber() {
		return PartNumber;
	}

	public void setPartNumber(String PartNumber) {
		this.PartNumber = PartNumber;
	}

	
}
