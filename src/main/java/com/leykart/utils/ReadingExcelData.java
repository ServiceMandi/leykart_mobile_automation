package com.leykart.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class ReadingExcelData {

	private static Workbook workbook = null;

	public static Workbook getWorkBook() {

		try {
			if (workbook == null) {
				String newFilePath = System.getProperty("user.dir") + "\\Leykart_Input.xls";
				System.out.println("New file path :::" + newFilePath);
				FileInputStream fs = new FileInputStream(newFilePath);
				workbook = Workbook.getWorkbook(fs);
			}

		} catch (Exception e) {

		}
		return workbook;
	}

	public static void readingWorkFlowData() throws IOException, BiffException {

		try {
			if (Utils.excelInputDataList == null) {
				Sheet sheet = getWorkBook().getSheet("WorkFlowData");

				List<String> ExpectedColumns = new ArrayList<String>();

				int masterSheetColumnIndex = sheet.getColumns();

				for (int x = 0; x < masterSheetColumnIndex; x++) {
					Cell celll = sheet.getCell(x, 0);
					String d = celll.getContents();
					ExpectedColumns.add(d);
				}

				LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

				List<String> column1 = new ArrayList<String>();

				/// read values from driver sheet for each column
				for (int j = 0; j < masterSheetColumnIndex; j++) {
					column1 = new ArrayList<String>();
					for (int i = 1; i < sheet.getRows(); i++) {
						Cell cell = sheet.getCell(j, i);
						column1.add(cell.getContents());
					}
					columnDataValues.put(ExpectedColumns.get(j), column1);
				}

				List<String> mobileNumberList = columnDataValues.get("MobileNumber");
				List<String> loginTypeNameList = columnDataValues.get("LoginTypeName");
				List<String> nameList = columnDataValues.get("Name");
				List<String> rgistrationNumberList = columnDataValues.get("RgistrationNumber");
				List<String> chassisNumberList = columnDataValues.get("ChassisNumber");
				List<String> aggregatesList = columnDataValues.get("Aggregates");
				List<String> modelLookupList = columnDataValues.get("ModelLookup");
				List<String> partNumberList = columnDataValues.get("PartNumber");
				
				ArrayList<ExcelInputData> userDetailsList = new ArrayList<ExcelInputData>();

				for (int i = 0; i < mobileNumberList.size(); i++) {
					ExcelInputData appInputDetail = new ExcelInputData();
					userDetailsList.add(appInputDetail);
					userDetailsList.get(i).setMobileNumber(mobileNumberList.get(i));
					userDetailsList.get(i).setLoginTypeName(loginTypeNameList.get(i));
					userDetailsList.get(i).setName(nameList.get(i));
					userDetailsList.get(i).setRgistrationNumber(rgistrationNumberList.get(i));
					userDetailsList.get(i).setChassisNumber(chassisNumberList.get(i));
					userDetailsList.get(i).setAggregates(aggregatesList.get(i));
					userDetailsList.get(i).setModelLookup(modelLookupList.get(i));
					userDetailsList.get(i).setPartNumber(partNumberList.get(i));
					
				}

				Utils.excelInputDataList = userDetailsList;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static LinkedHashMap<String, List<String>> readingFleetManagerData(String sheetName) {

		try {

			Sheet sheet = getWorkBook().getSheet(sheetName);
			List<String> ExpectedColumns = new ArrayList<String>();

			int masterSheetColumnIndex = sheet.getColumns();

			for (int x = 0; x < masterSheetColumnIndex; x++) {
				Cell celll = sheet.getCell(x, 0);
				String d = celll.getContents();
				ExpectedColumns.add(d);
			}

			LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

			List<String> column1 = new ArrayList<String>();
			/// read values from user edit sheet for each column
			for (int j = 0; j < masterSheetColumnIndex; j++) {
				column1 = new ArrayList<String>();
				for (int i = 1; i < sheet.getRows(); i++) {
					Cell cell = sheet.getCell(j, i);
					column1.add(cell.getContents());
				}
				columnDataValues.put(ExpectedColumns.get(j), column1);
			}
			return columnDataValues;
		} catch (Exception e) {

		}
		return null;
	}

	public static void readingNegativeData() throws IOException, BiffException {

		if (Utils.excelInputDataList == null) {
			Sheet sheet = getWorkBook().getSheet("NegativeWorkFlow");

			List<String> ExpectedColumns = new ArrayList<String>();

			int masterSheetColumnIndex = sheet.getColumns();

			for (int x = 0; x < masterSheetColumnIndex; x++) {
				Cell celll = sheet.getCell(x, 0);
				String d = celll.getContents();
				ExpectedColumns.add(d);
			}

			LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

			List<String> column1 = new ArrayList<String>();

			/// read values from driver sheet for each column
			for (int j = 0; j < masterSheetColumnIndex; j++) {
				column1 = new ArrayList<String>();
				for (int i = 1; i < sheet.getRows(); i++) {
					Cell cell = sheet.getCell(j, i);
					column1.add(cell.getContents());
				}
				columnDataValues.put(ExpectedColumns.get(j), column1);
			}

			List<String> mobileNumberList = columnDataValues.get("MobileNumber");
			List<String> loginTypeNameList = columnDataValues.get("LoginTypeName");
			List<String> nameList = columnDataValues.get("Name");
			List<String> rgistrationNumberList = columnDataValues.get("RgistrationNumber");
			List<String> chassisNumberList = columnDataValues.get("ChassisNumber");
			List<String> aggregatesList = columnDataValues.get("Aggregates");
			List<String> modelLookupList = columnDataValues.get("ModelLookup");
			List<String> partNumberList = columnDataValues.get("PartNumber");

			ArrayList<ExcelInputData> userDetailsList = new ArrayList<ExcelInputData>();

			for (int i = 0; i < mobileNumberList.size(); i++) {
				ExcelInputData appInputDetail = new ExcelInputData();
				userDetailsList.add(appInputDetail);
				userDetailsList.get(i).setMobileNumber(mobileNumberList.get(i));
				userDetailsList.get(i).setLoginTypeName(loginTypeNameList.get(i));
				userDetailsList.get(i).setName(nameList.get(i));
				userDetailsList.get(i).setRgistrationNumber(rgistrationNumberList.get(i));
				userDetailsList.get(i).setChassisNumber(chassisNumberList.get(i));
				userDetailsList.get(i).setAggregates(aggregatesList.get(i));
				userDetailsList.get(i).setModelLookup(modelLookupList.get(i));
				userDetailsList.get(i).setPartNumber(partNumberList.get(i));
			}

			Utils.excelInputDataList = userDetailsList;

		}
	}
}
