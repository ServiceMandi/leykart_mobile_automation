package com.leykart.common;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.leykart.utils.ExcelInputData;
import com.leykart.utils.Utils;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.TouchAction;

public class LeyKart extends Configuration {
	
	public void login(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException {
		
				try {
			    System.out.println("Boolean Value"+isVerified);
				leyKartCapabilities();
				Utils.sleepTimeLow();
	
				try {
					leyKarDriver.findElement(By.id("mbleno")).click();
					leyKarDriver.findElement(By.id("mbleno")).sendKeys("8122605545");
				 }catch(Exception e) {
				 }
							 
			 leyKarDriver.findElement(By.id("submituser")).click();	 

			    switchToLeyKartWebView();
				Utils.sleepTimeLow();
			    leyKarDriver.findElement(By.xpath("//button[contains(text(),'1')]")).click();
				leyKarDriver.findElement(By.xpath("//button[contains(text(),'1')]")).click();
				leyKarDriver.findElement(By.xpath("//button[contains(text(),'1')]")).click();
				leyKarDriver.findElement(By.xpath("//button[contains(text(),'1')]")).click();
				
				try {
					leyKarDriver.findElement(By.xpath("//html/body/div[4]/div[2]/div/div/button")).click();
				}catch(Exception e) {	
				}
				
				leyKarDriver.findElement(By.id("searchpage")).click();
				
				leyKarDriver.findElement(By.id("Aggregate_Search")).click();
				try {
				leyKarDriver.findElement(By.xpath("//html/body/ion-side-menus/ion-side-menu-content/ion-nav-view/ion-view[4]/ion-content/div/div/div/div[1]/h4")).click();
				leyKarDriver.findElement(By.xpath("//ion-nav-view/ion-view[3]/ion-content/div/div/div/div[1]")).click();
				}catch(Exception e) {
					switchToLeyKartWebView();
					leyKarDriver.findElement(By.xpath("//html/body/ion-side-menus/ion-side-menu-content/ion-nav-view/ion-view[5]/ion-content/div/div/div[1]")).click();	
				}
				try {
				leyKarDriver.findElement(By.xpath("//*[@id='div_0']")).click();
				}catch(Exception e) {
					leyKarDriver.findElement(By.xpath("//*[@id='div_0']/h4")).click();	
				}
				leyKarDriver.findElement(By.xpath("//*[@id='idforFont']/ion-list[1]/div/label")).click();
				
				leyKarDriver.findElement(By.xpath("//html/body/div[4]/div/div[2]/footer/div/button[1]")).click();
				
				
				
				
				leyKarDriver.findElement(By.id("wishitem")).click();
				
				switchToLeyKartWebView();
				
				leyKarDriver.findElement(By.id("searchpage")).click();
				leyKarDriver.findElement(By.id("wishitem")).click();
					
					 try {
							leyKarDriver.findElement(By.id("mbleno")).click();
							leyKarDriver.findElement(By.id("mbleno")).sendKeys("7305686048");
						 }catch(Exception e) {
							 leyKarDriver.findElement(By.id("submituser")).sendKeys("8122605545");
						 }
					 leyKarDriver.findElement(By.id("submituser")).click();	 
					 
					 leyKarDriver.findElement(By.xpath("//ion-nav-view/div/ion-view/ion-content/div/div[2]/div[1]/div")).click();
						
					leyKarDriver.findElement(By.xpath("//span[contains(text(),'Start Search')]")).click();
					 
					    switchToLeyKartWebView();
						Utils.sleepTimeLow();
					    leyKarDriver.findElement(By.xpath("//button[contains(text(),'1')]")).click();
						leyKarDriver.findElement(By.xpath("//button[contains(text(),'1')]")).click();
						leyKarDriver.findElement(By.xpath("//button[contains(text(),'1')]")).click();
						leyKarDriver.findElement(By.xpath("//button[contains(text(),'1')]")).click();
						
						try {
							leyKarDriver.findElement(By.xpath("//html/body/div[4]/div[2]/div/div/button")).click();
						}catch(Exception e) {	
						}
						
						leyKarDriver.findElement(By.id("searchpage")).click();
						
					System.out.println("Welcome to leykart");
					System.out.println("Enter the LeyKart Login method ::: ");
					//Thread.sleep(2000);
					String leyKarAppVerion_Path="//html/body/ion-side-menus/ion-side-menu-content/ion-nav-view/ion-view/ion-content/div/span";
					WebElement LeyKartVersion=leyKarDriver.findElement(By.xpath(leyKarAppVerion_Path));
					String leyKartAppVersion=LeyKartVersion.getText();
					System.out.println("LeyKart App Version ::"+leyKartAppVersion);
					Thread.sleep(100);
					
					
					
					
//					leyKarDriver.findElement(By.xpath("//html/body/ion-side-menus/ion-side-menu-content/ion-nav-view/div/ion-view/ion-content/div/div[2]/div[1]/div/img")).click();
//					leyKarDriver.findElement(By.xpath("//span[contains(text(),'Start Search')]")).click();
					
					//----------------------------------------------------------------------------------------------------------------------
					
					try{
							
						leyKarDriver.findElement(By.xpath("//select[@class='state-box ng-untouched ng-valid ng-dirty ng-valid-parse']")).click();
						leyKarDriver.findElement(By.xpath("//html/body/ion-side-menus/ion-side-menu-content/ion-nav-view/ion-view/ion-content/div/div[2]/select")).click();
						}catch(Exception e){
						try{
						leyKarDriver.findElement(By.xpath("//html/body/ion-side-menus/ion-side-menu-content/ion-nav-view/ion-view/ion-content/div/div[2]/select")).click();
						leyKarDriver.findElement(By.xpath("//html/body/ion-side-menus/ion-side-menu-content/ion-nav-view/ion-view/ion-content/div/div[2]/select/option[1]")).click();
						}catch(Exception e1){
						try{
						leyKarDriver.findElement(By.xpath("//html/body/ion-side-menus/ion-side-menu-content/ion-nav-view/ion-view/ion-content/div/div[2]/select/option[1]")).click();
						}catch(Exception e2){
						}
						}
						}
					
					List<WebElement> languageList = leyKarDriver.findElements(By.xpath(("//ion-side-menu-content/ion-nav-view/ion-view/ion-content/div/div[2]/select/*")));
					System.out.println("Chacking language List size :::" + languageList.size());
					
					for(int i=0;i<languageList.size();i++) {
						String langauage=languageList.get(i).getText();
						
						System.out.println("The language name :::" + langauage);						
						if(i==0){
							System.out.println("Button selected :::"+ languageList.get(i).isEnabled());
							//System.out.println("checked :::" + languageList.get(i).getAttribute("checked"));
							languageList.get(i).click();
						    //System.out.println("Language Path :"+languageList);
							System.out.println("Button selected :::"+ languageList.get(i).isSelected());
							System.out.println("Button selected 222 :::"+ languageList.get(i).isDisplayed());
							leyKarDriver.findElement(By.xpath("//html/body/ion-side-menus/ion-side-menu-content/ion-nav-view/ion-view/ion-content/div/div[2]/select")).click();
							//return;
						}
							
				}
					
					 try {
						leyKarDriver.findElement(By.xpath("//input[@id='mbleno']")).click();
						leyKarDriver.findElement(By.xpath("//input[@id='mbleno']")).sendKeys("9677131113");
					 }catch(Exception e) {
						 leyKarDriver.findElement(By.xpath("//input[@name='usermobileno']")).sendKeys("8122605545");
					 }
						     
						Thread.sleep(100);
						
						//leyKarDriver.SwitchTo().DefaultContent();
						
						//leyKarDriver.findElementByXPath("//button[@id='autoButton']").click();
						
//						WebElement login=leyKarDriver.findElement(By.id("loginBtn"));
//						if (login != null) {
//							String submitbtn=login.getText();
//							System.out.println("Submit Text ::"+submitbtn);
//						} else {
//							System.out.println("Button not exists");
//						}
//						
//						leyKarDriver.findElement(By.id("loginBtn")).click();
						leyKarDriver.findElementByXPath("//button[contains(text(),'Submit')]").click();
						
						//leyKarDriver.findElement(By.xpath("//button[contains(text(),'Submit')]")).sendKeys(Keys.RETURN);
						//leyKarDriver.findElement(By.xpath("//button[contains(text(),'Submit')]]")).sendKeys(Keys.ENTER);
						
						WebElement webElement = leyKarDriver.findElement(By.xpath("//button[contains(text(),'Submit')]"));
						Actions builder = new Actions(leyKarDriver);
						builder.moveToElement(webElement).click(webElement);
						builder.perform();
						
						Thread.sleep(1000);
						//WebElement webElement1 = leyKarDriver.findElement(By.id("Your ID here"));
						JavascriptExecutor executor = (JavascriptExecutor) leyKarDriver;
						executor.executeScript("arguments[0].click();", webElement);
						
//						TouchActions taction = new TouchActions(leyKarDriver);
//						taction.longPress(webElement);
//						taction.perform();
						
						WebDriverWait wait = new WebDriverWait(leyKarDriver, 2);
						wait.until(ExpectedConditions.elementToBeClickable(webElement));
						
						leyKarDriver.findElement(By.xpath("//button[@data-ink-opacity='1']")).click();
						leyKarDriver.findElement(By.xpath("//button[contains(text(),'Submit')]")).click();
						//leyKarDriver.findElement(By.xpath("//html/body/ion-side-menus/ion-side-menu-content/ion-nav-view/ion-view[1]/ion-content/div/div[4]")).click();
						Actions action = new Actions(leyKarDriver);
						WebElement submit=leyKarDriver.findElement(By.xpath("//button[contains(text(),'Submit')]"));
						action.doubleClick(submit).build().perform();
						WebElement submit1=leyKarDriver.findElement(By.xpath("//button[@data-ink-opacity='1']"));
						action.doubleClick(submit1).build().perform();
						
						leyKarDriver.findElement(By.xpath("//button[contains(text(),'1')]")).click();
						leyKarDriver.findElement(By.xpath("//button[contains(text(),'2')]")).click();
						leyKarDriver.findElement(By.xpath("//button[contains(text(),'3')]")).click();
						leyKarDriver.findElement(By.xpath("//button[contains(text(),'4')]")).click();
						
						leyKarDriver.findElement(By.xpath("//button[contains(text(),'CONTINUE')]")).click();
						//leyKarDriver.findElement(By.xpath("//html/body/ion-side-menus/ion-side-menu-content/ion-nav-view/ion-view[2]/div/button")).click();
						
						leyKarDriver.findElement(By.xpath("//button[contains(text(),'Add to Cart')]")).click();
						
						
						
						leyKarDriver.findElement(By.xpath("//html/body/ion-side-menus/ion-side-menu-content/ion-nav-view/div/ion-view/ion-content/div/div[2]/div[1]/div")).click();
						
						leyKarDriver.findElement(By.xpath("//*[@id='hambermenu']")).click();
						
						leyKarDriver.findElement(By.xpath("//span[contains(text(),'Logout')]")).click();
						try {
						leyKarDriver.findElement(By.xpath("//span[contains(text(),'My Garage')]")).click();
						//leyKarDriver.findElement(By.xpath("//button[@class='button back-button hide buttons button-icon icon button-clear ion-arrow-left-c custom-back-button header-item']")).click();
						leyKarDriver.findElement(By.xpath("//span[contains(text(),'Start Search')]")).click();
						}catch(Exception e) {
							
						}
					
					//-------------------------------------------------------------------------------------------------------------------------------------------
						
						
					
					leyKarDriver.findElement(By.cssSelector("body > ion-side-menus > ion-side-menu-content > ion-nav-view > ion-view:nth-child(1) > ion-content > div > form > div > div > ion-list > div > label:nth-child(1)")).click();
					
					tapOnCoordinates(328, 58);
					
					WebElement englishBtn = leyKarDriver.findElement(By.xpath("//label[@value='english']"));
					((JavascriptExecutor) leyKarDriver).executeScript("arguments[0].scrollIntoView(true);", englishBtn);
					
					
	                //Using List
					List<WebElement> languageList3 = leyKarDriver.findElements(By.xpath(("//ion-content/div/form/div/div/ion-list/div/*")));
					System.out.println("Chacking language List size :::" + languageList3.size());
					
							if( leyKarDriver.findElement(By.xpath("//label[@value='english']")).isSelected())
							{
							//do something here.
								leyKarDriver.findElement(By.xpath("//label[@value='english']")).click();
								
							}
							leyKarDriver.findElement(By.xpath("//label[@value='english']")).click();
					
					
					//languageList.get(0).click();
					
//					for(int i=0;i<languageList.size();i++) {
//						String langauage=languageList.get(i).getText();
//						
//						System.out.println("The language name :::" + langauage);						
//						//if(langauage.equalsIgnoreCase("English")){
//							System.out.println("Button selected :::"+ languageList.get(i).isEnabled());
//							//System.out.println("checked :::" + languageList.get(i).getAttribute("checked"));
//							languageList.get(i).click();
//						    //System.out.println("Language Path :"+languageList);
//							System.out.println("Button selected :::"+ languageList.get(i).isSelected());
//							System.out.println("Button selected 222 :::"+ languageList.get(i).isDisplayed());
//							return;
//						//}
//							
//					}
					
					//WebElement englishBtn = leyKarDriver.findElement(By.xpath("//label[@value='english']"));
					
					
					Point location =	leyKarDriver.findElement(By.xpath("//label[@value='english']")).getLocation();
					Dimension size	=leyKarDriver.findElement(By.xpath("//label[@value='english']")).getSize();
			        
//					Point location = englishBtn.getLocation();
//			        Dimension size = englishBtn.getSize();

			        // Find the center of this element where we we start the 'touch'
			        int x = location.getX() + (size.getWidth() / 2);
			        int y = location.getY() + (size.getHeight() / 2);
			        
			        System.out.println("X axis :"+x);
			        System.out.println("y axis :"+y);

			        //new TouchActions(leyKarDriver).down(x, y).move(150, 0).clickAndHold();
					
					//Normal
					leyKarDriver.findElement(By.xpath("//label[@value='english']")).click();
					leyKarDriver.findElement(By.xpath("//label[@value='hindi']")).click();
					leyKarDriver.findElement(By.xpath("//label[@value='tamil']")).click();
					
					//Double Click
//					Actions action = new Actions(leyKarDriver);
//					WebElement English=leyKarDriver.findElement(By.xpath("//label[@value='english']"));
//					WebElement Hindi=leyKarDriver.findElement(By.xpath("//label[@value='hindi']"));
//					WebElement Tamil=leyKarDriver.findElement(By.xpath("//label[@value='tamil']"));
//					
//					action.doubleClick(English).build().perform();
//					action.doubleClick(Hindi).build().perform();
//					action.doubleClick(Tamil).build().perform();
					
					//Long Press
					
//					WebElement englishBtn = leyKarDriver.findElement(By.xpath("//label[@value='english']"));
//					new TouchAction((MobileDriver) leyKarDriver).press(englishBtn).waitAction(Duration.ofMillis(10000)).release().perform();
//					
//					WebElement hindiBtn = leyKarDriver.findElement(By.xpath("//label[@value='hindi']"));
//					new TouchAction((MobileDriver) leyKarDriver).press(hindiBtn).waitAction(Duration.ofMillis(10000)).release().perform();
//					
//					WebElement tamilBtn = leyKarDriver.findElement(By.xpath("//label[@value='tamil']"));
//					new TouchAction((MobileDriver) leyKarDriver).press(tamilBtn).waitAction(Duration.ofMillis(10000)).release().perform();
//					
//					TouchAction action1 = new TouchAction(leyKarDriver);
//					action1.longPress(englishBtn).release().perform();
					
					leyKartWait(LocatorPath.lkMobileNumber);
					isElementPresentVerification(LocatorType.XPATH, LocatorPath.lkMobileNumber);
					clickLeyKartElement(LocatorType.XPATH, LocatorPath.lkMobileNumber);
					//enterLeyKartValue(LocatorType.XPATH, LocatorPath.lkMobileNumber, "7305686048");
					enterLeyKartValue(LocatorType.XPATH, LocatorPath.lkMobileNumber, excelInputData.getMobileNumber());
					hideKeyBoard(leyKarDriver);
					Thread.sleep(2000);
					clickLeyKartElement(LocatorType.XPATH, LocatorPath.requestLKOTP);
					Thread.sleep(2000);
					hideKeyBoard(leyKarDriver);
					Thread.sleep(1000);
					//clickLeyKartElement(LocatorType.XPATH, LocatorPath.loginButton);
					
					clickLeyKartElement(LocatorType.XPATH, LocatorPath.homePage_Button);
					
					scrollDownAppScreen();
					
					//writeReport(LogStatus.PASS, "LeyKart App Version::::::"+leyKartAppVersion);
					writeReport(LogStatus.PASS, "LeyKart Login Pass");
				} catch (Exception e) {
					writeReport(LogStatus.ERROR, "LeyKart Login Failed");
					e.printStackTrace();
				}
			
	}
	
	public void loginiAlert() throws InterruptedException {
		
		try {
	    iAlertCapabilities();
		Utils.sleepTimeLow();

		    switchToiAlertWebView();
			Utils.sleepTimeLow();
			
			
			
		} catch (Exception e) {
			writeReport(LogStatus.ERROR, "LeyKart Login Failed");
			e.printStackTrace();
		}
	
}
	

}
