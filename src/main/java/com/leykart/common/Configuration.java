package com.leykart.common;

import java.io.File;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.leykart.utils.Utils;
import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class Configuration {

	private static final String String = null;
	private static DesiredCapabilities desiredCapabilities;
	private WebElement element;
	protected static AndroidDriver leyKarDriver = null;
	protected static AndroidDriver iAlertDriver = null;
	protected static AndroidDriver mechanicDriver = null;
	protected static AndroidDriver driveDriver = null;
	protected static WebDriverWait leyKartWait = null;
	protected static WebDriverWait mechWait = null;
	protected static WebDriverWait driverWait = null;
	// protected static Wait<WebDriver> fmWait;
	// protected static Wait<WebDriver> mechWait;
	public static ExtentReports extentReports;
	public static ExtentTest eLogger;
	public static String leyKartAppPackage = "WEBVIEW_com.al.leykart";
	public static String mechAppPackage = "WEBVIEW_com.servicemandi.mechanic";
	public static String driveAppPackage = "WEBVIEW_com.servicemandi.driver";
	
	public static String iAlertAppPackage = "WEBVIEW_com.al.ialert.uat";
	public static String iAlertNativeApp = "NATIVE_App";
	
	
	

	public enum LocatorType {
		XPATH, ID, CLASS, NAME, CSS
	}

	public static void leyKartCapabilities() {

		try {

			desiredCapabilities = new DesiredCapabilities();
			desiredCapabilities.setCapability("deviceName", "Samsung SM-J120G");

			desiredCapabilities.setCapability("appium-version", "1.7.1");
			//desiredCapabilities.setCapability(MobileCapabilityType.UDID, "emulator-5554"); // Deepa
			desiredCapabilities.setCapability(MobileCapabilityType.UDID,"G1AZGU0300233YM"); // Santa
		    //desiredCapabilities.setCapability(MobileCapabilityType.UDID,"4200d37af0b41401"); // Priya
			// FM
			//desiredCapabilities.setCapability(MobileCapabilityType.UDID,"420015ccb2881459"); // New Device
			desiredCapabilities.setCapability("platformName", "Android");
			desiredCapabilities.setCapability("platformVersion", "5.1.1");
			desiredCapabilities.setCapability("recreateChromeDriverSessions", "true");
			desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100000");
			//desiredCapabilities.setCapability("appPackage", "com.servicemandi.fm");
			//desiredCapabilities.setCapability("appPackage", "com.al.leykart_qa_env");
			desiredCapabilities.setCapability("appPackage", "com.al.leykart"); 
			//desiredCapabilities.setCapability("appActivity", "com.servicemandi.fm.MainActivity");
			//desiredCapabilities.setCapability("appActivity", "com.al.leykart_qa_env.MainActivity");
			desiredCapabilities.setCapability("appActivity", "com.al.leykart.MainActivity");
			//desiredCapabilities.setCapability("automationName", "uiautomator2");
			desiredCapabilities.setCapability("autoAcceptAlerts", true);
			desiredCapabilities.setCapability("autoDismissAlerts", true);

			leyKarDriver = new AndroidDriver(new URL("http://127.0.0.1:4273/wd/hub"), desiredCapabilities);

			leyKarDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			leyKartWait = new WebDriverWait(leyKarDriver, 120);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error ::" + e.toString());
		}

	}
	
	public static void iAlertCapabilities() {

		try {

			desiredCapabilities = new DesiredCapabilities();
			desiredCapabilities.setCapability("deviceName", "Samsung SM-J120G");

			desiredCapabilities.setCapability("appium-version", "1.7.1");
			//desiredCapabilities.setCapability(MobileCapabilityType.UDID, "emulator-5554"); // Deepa
			desiredCapabilities.setCapability(MobileCapabilityType.UDID,"420015ccb2881459"); // Santa
		    //desiredCapabilities.setCapability(MobileCapabilityType.UDID,"4200d37af0b41401"); // Priya
			// FM
			//desiredCapabilities.setCapability(MobileCapabilityType.UDID,"420015ccb2881459"); // New Device
			desiredCapabilities.setCapability("platformName", "Android");
			desiredCapabilities.setCapability("platformVersion", "5.1.1");
			desiredCapabilities.setCapability("recreateChromeDriverSessions", "true");
			desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100000");
			//desiredCapabilities.setCapability("appPackage", "com.servicemandi.fm");
			desiredCapabilities.setCapability("appPackage", "com.al.ialert.uat");
			//desiredCapabilities.setCapability("appPackage", "com.al.leykart"); 
			//desiredCapabilities.setCapability("appActivity", "com.servicemandi.fm.MainActivity");
			desiredCapabilities.setCapability("appActivity", "com.al.ialert.uat.MainActivity");
			//desiredCapabilities.setCapability("appActivity", "com.al.leykart.MainActivity");
			//desiredCapabilities.setCapability("automationName", "uiautomator2");
			desiredCapabilities.setCapability("autoAcceptAlerts", true);
			desiredCapabilities.setCapability("autoDismissAlerts", true);

			iAlertDriver = new AndroidDriver(new URL("http://127.0.0.1:4273/wd/hub"), desiredCapabilities);

			iAlertDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			leyKartWait = new WebDriverWait(iAlertDriver, 120);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error ::" + e.toString());
		}

	}

	public static AndroidDriver getLeyKartInstance() {

		if (leyKarDriver != null) {
			return leyKarDriver;
		}
		return null;
	}


	public static void reportConfiguration() throws InterruptedException {

		if (extentReports == null) {
			extentReports = new ExtentReports(
					System.getProperty("user.dir") + File.separator + "LeyKart_Reports.html", true,
					DisplayOrder.OLDEST_FIRST);
		}
		
//		System.out.println("WorkflowName::::" + newWorkFlow);
//		DateFormat df = new SimpleDateFormat("dd MM yyyy");
//		Thread.sleep(2000);
//		System.out.println("DateFormat::::" + df.format(new Date()));
//		///C:\\Users\\14402\\git\\demoservicemandiautomation\\SM_Report\\
//		extentReports = new ExtentReports("D:\\Protactor\\Report" + newWorkFlow + df.format(new Date()) + ".html",true,DisplayOrder.OLDEST_FIRST);
//		//extentReports = new ExtentReports("D:\\SMReports\\" + newWorkFlow + ".html",true,DisplayOrder.OLDEST_FIRST);
//		return newWorkFlow;

	}

	public static void closeWorkFlowReport() {
		extentReports.endTest(eLogger);
	}

	public static void createNewWorkFlowReport(String newWorkFlow) {

		eLogger = extentReports.startTest(newWorkFlow);
	}

	public WebElement getLeyKartWebElement(LocatorType type, String locator) {
		element = null;
		switch (type) {
		case XPATH:
			if (leyKarDriver.findElements(By.xpath(locator)).size() > 0) {
				element = leyKarDriver.findElement(By.xpath(locator));
			} else {
				element = null;
			}
			break;
		case NAME:
			if (leyKarDriver.findElements(By.name(locator)).size() > 0) {
				element = leyKarDriver.findElement(By.name(locator));
			} else {
				element = null;
			}
			break;
		case CLASS:
			if (leyKarDriver.findElements(By.className(locator)).size() > 0) {
				element = leyKarDriver.findElement(By.className(locator));
			} else {
				element = null;
			}
			break;
		case CSS:
			if (leyKarDriver.findElements(By.cssSelector(locator)).size() > 0) {
				element = leyKarDriver.findElement(By.cssSelector(locator));
			} else {
				element = null;
			}
			break;
		}
		return element;
	}

	public void clickLeyKartElement(LocatorType type, String locator) {

		try {
			Thread.sleep(3000);
			WebElement elem = getLeyKartWebElement(type, locator);
			if (elem != null) {
				elem.click();
			}
			writeReport(LogStatus.PASS, " Click on"+elem.getText());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void enterLeyKartValue(LocatorType type, String locator, String data) {

		try {
			WebElement elem = getLeyKartWebElement(type, locator);
			if (elem != null) {
				elem.click();
				elem.clear();
				elem.sendKeys(data);
			}
		} catch (Exception e) {
		}
	}

	/**
	 * This method switches to web view context.
	 */
	public static void switchToLeyKartWebView() {

		try {
			Thread.sleep(2000);
			Set<String> availableContexts = leyKarDriver.getContextHandles();
			for (String context : availableContexts) {
				if (context.contains(leyKartAppPackage)) {
					leyKarDriver.context(context);
					System.out.println("Context switched to web view ::: " + context);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This method switches to web view context.
	 */
	public static void switchToiAlertWebView() {

		try {
			Thread.sleep(2000);
			Set<String> availableContexts = iAlertDriver.getContextHandles();
			for (String context : availableContexts) {
				if (context.contains(iAlertAppPackage)) {
					iAlertDriver.context(context);
					System.out.println("Context switched to web view ::: " + context);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void switchToiAlertNativeView() {

		try {
			Thread.sleep(2000);
			Set<String> availableContexts = iAlertDriver.getContextHandles();
			for (String context : availableContexts) {
				if (context.contains(iAlertNativeApp)) {
					iAlertDriver.context(context);
					System.out.println("Context switched to web view ::: " + context);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void leyKartWait(final String locator) throws InterruptedException {

		Thread.sleep(1000);
		leyKartWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		/*
		 * fmWait.until(new Function<WebDriver, WebElement>() { public
		 * WebElement apply(WebDriver driver) {
		 * 
		 * WebElement element = driver.findElement(By.xpath(locator)); String
		 * getTextOnPage = element.getText(); System.out.println(getTextOnPage);
		 * return null; } });
		 */
	}

	/**
	 * This method destroys the created android driver instance.
	 */
	public static void tearDownFleetManager() {
		try {
			Thread.sleep(5000);
			leyKarDriver.quit();
		} catch (InterruptedException e) {
		}
	}

	public static void saveReport() {

		eLogger.log(LogStatus.PASS, "End of Work Flow");
		extentReports.endTest(eLogger);
		extentReports.flush();
		extentReports.close();
	}

	

	public static String getElementTextValue(AndroidDriver driver, String locator) {

		try {
			return driver.findElement(By.xpath(locator)).getText();
		} catch (Exception e) {

		}
		return "";
	}

	public static String getElementAttributeValue(AndroidDriver driver, String locator) {

		try {
			return driver.findElement(By.xpath(locator)).getAttribute("value");
		} catch (Exception e) {

		}
		return "";
	}

	public static void writeReport(LogStatus logStatus, String status) throws InterruptedException {

		if (eLogger == null) {
			reportConfiguration();
		}

		status = status + " : " + Utils.timeCalculation();
		switch (logStatus) {

		case PASS:
			eLogger.log(LogStatus.PASS, status);
			break;
		case FAIL:
			eLogger.log(LogStatus.FAIL, status);
			break;
		case INFO:
			eLogger.log(LogStatus.INFO, status);
		}
	}

	public static void hideKeyBoard(AndroidDriver androidDriver) {

		try {
			androidDriver.hideKeyboard();
			Thread.sleep(1000);
		} catch (Exception e) {
			// Code change done by Shanthakumar
			e.printStackTrace();
		}
	}
	
	public static void takeSceenShot() throws InterruptedException{
		try {
			TakesScreenshot scrShot =((TakesScreenshot)leyKarDriver);
			Thread.sleep(2000);
			File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
			Thread.sleep(3000);
			String Filename=UUID.randomUUID().toString();
			DateFormat df1 = new SimpleDateFormat(" dd MM yyyy hh");
			String DestFile=System.getProperty("user.dir") +"_FM LOGIN Screenshot_"+Filename+ df1.format(new Date())+".png";
			System.out.println(DestFile);
			FileUtils.copyFile(SrcFile, new File(DestFile));
			eLogger.log(LogStatus.PASS, "LeyKart Screenshot::: " + eLogger.addScreenCapture(DestFile));
			
		} catch (Exception e) {
			writeReport(LogStatus.ERROR, "LayKart Screenshot Failed");
			e.printStackTrace();
		}
		
	}
	/**
	 ************************************************************* 
	 *Method - Method to verify if an element is present.
	 *Author -Rakesh
	 * @throws InterruptedException 
	 ************************************************************* 
	 */
	public void isElementPresentVerification(LocatorType type, String locator) throws InterruptedException {
		boolean isElementPresent = false;
		WebElement webElement = null;
		String element = null;
		try {
			webElement = getLeyKartWebElement(type, locator);
			element=webElement.getText();
			if (webElement != null) {
				isElementPresent = true;
				writeReport(LogStatus.PASS, element+" is present in screen");
			}
		} catch (Exception e) {
			writeReport(LogStatus.ERROR, element+" is not present in screen");
			e.printStackTrace();
		}
		
	}
	/**
	 ************************************************************* 
	 *Method -  Method to scroll up in mobile app.
	 *Author - Rakesh
	 ************************************************************* 
	 */
	public static void scrollUpAppScreen() {
		try {
			
			TouchActions action = new TouchActions(leyKarDriver);
//			action.press(400, 600).waitAction(Duration.ofSeconds(1)).moveTo(400, 100).release().perform();
			Thread.sleep(2000);
		} catch (Exception e) {
			
		}
	}
	
	
	/**
	 ************************************************************* 
	 *Method -  Method to scroll down in mobile app.
	 *Author - Rakesh
	 ************************************************************* 
	 */
	public static void scrollDownAppScreen() {
		try {
			Thread.sleep(3000);
			TouchAction action = new TouchAction(leyKarDriver);
		//	action.press(100, 500).waitAction(Duration.ofSeconds(1)).moveTo(600, 1000).release().perform();
			Thread.sleep(2000);
		} catch (Exception e) {
			
	}}
	/**
	 ************************************************************* 
	 *Method -   Method to close the mobile app.
	 *Author - Rakesh
	 ************************************************************* 
	 */
	public static void closeApp() {
		try {
			leyKarDriver.closeApp();
		} catch (Exception e) {
			
		}

	}
	
	/**
	 ************************************************************* 
	 *Method -   Method to perform the tap operation on given coordinates.
	 *Author - Rakesh
	 ************************************************************* 
	 */
	public static void tapOnCoordinates(int x, int y) {
		JavascriptExecutor js = (JavascriptExecutor) leyKarDriver;

		HashMap<String, Double> coords = new HashMap<String, Double>();

		coords.put("x", (double) x); // in pixels from left

		coords.put("y", (double) y); // in pixels from top

		js.executeScript("mobile: tap", coords);
	}
}
