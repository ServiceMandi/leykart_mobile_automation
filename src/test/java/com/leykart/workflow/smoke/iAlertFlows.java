package com.leykart.workflow.smoke;

import org.testng.annotations.Test;

import com.leykart.common.Configuration;
import com.leykart.common.LeyKart;
import com.leykart.utils.ReadingExcelData;
import com.leykart.utils.Utils;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;

public class iAlertFlows extends Configuration {
	
	private LeyKart iAlert;
	
  
  @BeforeMethod
  public void setUpConfiguration() {
	  
	  try {
		    iAlertCapabilities();
			Utils.sleepTimeLow();

			   switchToiAlertWebView();
				Utils.sleepTimeLow();
//				
//				//Enter UserName
//				iAlertDriver.findElement(By.xpath("//input[@ng-model='user.loginId']")).click();
//				iAlertDriver.findElement(By.xpath("//input[@ng-model='user.loginId']")).sendKeys("RLPT");
//				 
//				//Enter Password
//				iAlertDriver.findElement(By.xpath("//input[@ng-model='user.password']")).click();
//				iAlertDriver.findElement(By.xpath("//input[@ng-model='user.password']")).sendKeys("Starshine@%^&");
//				
//				//Click on Log On Button
//				
				
				iAlertDriver.findElement(By.id("loginTextField")).click();
				iAlertDriver.findElement(By.id("loginTextField")).sendKeys("RLPT");
				
				iAlertDriver.findElement(By.id("passwordTextField")).click();
				iAlertDriver.findElement(By.id("passwordTextField")).sendKeys("Starshine@%^&");
				
				switchToiAlertNativeView();
				Utils.sleepTimeLow();
				iAlertDriver.findElement(By.id("lOGIN-button123")).click();
				iAlertDriver.findElement(By.cssSelector("#lOGIN-button123")).click();
				
				iAlertDriver.findElement(By.id("lOGIN-button124")).click();
				
				iAlertDriver.findElement(By.id("lOGIN-button125")).click();

				WebElement element = iAlertDriver.findElement(By.id("lOGIN-button123"));
				JavascriptExecutor executor = (JavascriptExecutor)iAlertDriver;
				executor.executeScript("arguments[0].click();", element);
				
				executor.executeScript("document.getElementById('lOGIN-button123').click();");
				
				executor.executeScript("window.document.getElementById('lOGIN-button123').click()");
				
				WebElement nameInputField = iAlertDriver.findElement(By.xpath("//a[contains(text(),'Log On')]"));
				JavascriptExecutor executor1 = (JavascriptExecutor)iAlertDriver;
				executor1.executeScript("arguments[0].click();", nameInputField);
				
			} catch (Exception e) {
				writeReport(LogStatus.ERROR, "LeyKart Login Failed");
				e.printStackTrace();
			}
	  
  }
   @Test
  public void iAlertHome() {
	  System.out.println("Run First Test");
  }
  

  @AfterMethod
  public void afterMethod() {
	  
	  Configuration.closeWorkFlowReport();
  }

}
