package com.leykart.workflow.smoke;

import org.testng.annotations.AfterMethod;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.leykart.common.Configuration;
import com.leykart.common.LeyKart;
import com.leykart.utils.ReadingExcelData;
import com.leykart.utils.Utils;

public class LeyKartFlows<ReqNumber>  {
	
	private LeyKart leykart;
	boolean isVerified = true;
	@BeforeTest
	public void setUpConfiguration() {

		try {

			ReadingExcelData.readingWorkFlowData();
			leykart = new LeyKart();
			Configuration.reportConfiguration();
			Configuration.createNewWorkFlowReport("Login Details");
			leykart.login(Utils.excelInputDataList.get(0), isVerified);
			Configuration.closeWorkFlowReport();

		} catch (Exception e) {

		}
	}
	@AfterTest
	public void afterTest() {
		Configuration.saveReport();
	}
	@AfterMethod
	public void afterMethod() {
		Configuration.closeWorkFlowReport();
	}
	
  @Test
  public void leykartHome() {
	  try{
		  if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Leykart Flows=====");
				Configuration.createNewWorkFlowReport("LeyKart Flows");
	            System.out.println("Run First Test");
	            Configuration.closeWorkFlowReport();
		  }
	  }catch(Exception e){
		  
	  }
  }
}
